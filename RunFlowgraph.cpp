//misc includdes
#include <iostream>
#include <string>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <memory>

//#include <gnuradio/constants.h> // version
//#include <digitizers/constants.h> // version
//#include <flowgraph/constants.h> // version
#include <flowgraph/flowgraph.h>

#include <cmath>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <utility>
#include <stdexcept>
#include <limits>

// saftlib
#include <giomm.h>
#include <SAFTd.h>
#include <TimingReceiver.h>
#include <SoftwareActionSink.h>
#include <SoftwareCondition.h>
#include <SCUbusCondition.h>
#include <SCUbusActionSink.h>
#include <iDevice.h>
#include <Output.h>
#include <Input.h>
#include <OutputCondition.h>

// GLib
#include <giomm.h>

const uint32_t MAX_FLOWGRAPH_XML_LENGTH = 1000000;

#define TIMING_FORMAT_ID 1

#define EVENTID_FORMATID_OFFSET 60
//FormatID 0
#define EVENTID_GROUPID_OFFSET 48
#define EVENTID_EVENTNO_OFFSET 36
#define EVENTID_SEQUENCE_INDEX_OFFSET 24
#define EVENTID_PROCESS_INDEX_OFFSET 10
#define EVENTID_SEQCOUNT_OFFSET 0   // deprecated
#define EVENTID_RESERVED_OFFSET 0

#define PARAM_CHAIN_INDEX_OFFSET 42
#define PARAM_CHAIN_TIME_OFFSET 0
#define PARAM_CHAIN_INDEX_MASK 0x3fffff
#define PARAM_CHAIN_TIME_MASK 0x3ffffffffff

#define EVENTID_FORMATID_MASK 0xf
#define EVENTID_GROUPID_MASK 0xfff
#define EVENTID_EVENTNO_MASK 0xfff
#define EVENTID_FLAGS_MASK 0xf
#define EVENTID_SEQID_MASK 0xfff
#define EVENTID_BPID_MASK 0x3fff
#define EVENTID_SEQCOUNT_MASK 0x3ff  // deprecated
#define EVENTID_RESERVED_MASK 0x3f

#define EVENTID_GROUPID_OFFSET_V1 48
#define EVENTID_EVENTNO_OFFSET_V1 36
#define EVENTID_FLAGS_OFFSET_V1 32
#define EVENTID_FLAGS_BEAMIN_OFFSET_V1 35
#define EVENTID_SEQUENCE_INDEX_OFFSET_V1 20
#define EVENTID_PROCESS_INDEX_OFFSET_V1 6
#define EVENTID_RESERVED_OFFSET_V1 0

#define RUNTIME_ERROR(x) std::runtime_error(__FILE__  + std::string(":") + std::to_string(__LINE__) + " Error: " + x )


uint32_t extractEventID(const std::string& eventString)
{
    size_t separator = eventString.find("#");
    if (separator == std::string::npos)
    {
        throw RUNTIME_ERROR("No seperator found");
    }
    std::string eventIDString = eventString.substr(separator + 1, std::string::npos  );

    std::istringstream iss(eventIDString);

    uint32_t eventID;
    iss >> eventID;
    return eventID;
}

uint64_t generate_event_id_for_fid(int format_id, int group_id, int event_no)
{
    uint64_t event_id = static_cast<uint64_t>(format_id) << EVENTID_FORMATID_OFFSET;
    if (format_id == 1)
    {
        event_id |= ((uint64_t) (group_id & EVENTID_GROUPID_MASK)) << EVENTID_GROUPID_OFFSET_V1;
        event_id |= ((uint64_t) (event_no & EVENTID_EVENTNO_MASK)) << EVENTID_EVENTNO_OFFSET_V1;
    }
    else if (format_id == 0)
    {
        event_id |= ((uint64_t) (group_id & EVENTID_GROUPID_MASK)) << EVENTID_GROUPID_OFFSET;
        event_id |= ((uint64_t) (event_no & EVENTID_EVENTNO_MASK)) << EVENTID_EVENTNO_OFFSET;
    }
    return event_id;
}

uint64_t generate_event_id(int group_id, int event_no)
{
    return generate_event_id_for_fid(TIMING_FORMAT_ID, group_id, event_no);
}

uint64_t generate_event_id_mask_for_fid(int format_id, int group_id, int event_no)
{
    uint64_t mask = ((uint64_t)EVENTID_FORMATID_MASK) << EVENTID_FORMATID_OFFSET;
    if (format_id==1 || format_id==0)
    {
        if (group_id != 0)
        {
            mask |= ((uint64_t)EVENTID_GROUPID_MASK) << EVENTID_GROUPID_OFFSET;
        }
        if (event_no != 0)
        {
            mask |= ((uint64_t)EVENTID_EVENTNO_MASK) << EVENTID_EVENTNO_OFFSET;
        }
    }
    return mask;
}

uint64_t generate_event_id_mask(int group_id, int event_no)
{
    return generate_event_id_mask_for_fid(TIMING_FORMAT_ID, group_id, event_no);
}


std::shared_ptr<saftlib::Output_Proxy> getOutputProxy(const std::string &devName, const std::string &ioName)
{
    auto devices = saftlib::SAFTd_Proxy::create()->getDevices();
    if (devices.empty())
    {
        throw RUNTIME_ERROR("no timing device present");
    }

    auto it = devices.find(devName);
    if (it == devices.end())
    {
        throw RUNTIME_ERROR("can't find timing device: " + devName);
    }

    auto receiver = saftlib::TimingReceiver_Proxy::create(it->second);
    auto outputs = receiver->getOutputs();

    auto output_it = outputs.find(ioName);
    if (output_it == outputs.end())
    {
        throw RUNTIME_ERROR("can't find IO " + ioName);
    }

    return saftlib::Output_Proxy::create(output_it->second);
}

    void configureOutput(const std::string &devName, const std::string &ioName)
    {
        auto output_proxy = getOutputProxy(devName, ioName);

        output_proxy->setOutputEnable(true);
        output_proxy->WriteOutput(false);
    }

    void createOutputCondition(const std::string &devName, const std::string &ioName, guint64 eventID, guint64 eventMask, gint64 offset, bool ioEdge)
    {
        auto output_proxy = getOutputProxy(devName, ioName);

        // Setup condition
        auto condition = saftlib::OutputCondition_Proxy::create(output_proxy->NewCondition(true, eventID, eventMask, offset, ioEdge));

        condition->setAcceptConflict(false);
        condition->setAcceptDelayed(true);
        condition->setAcceptEarly(true);
        condition->setAcceptLate(true);
    }

    void destroyOutputConditions(const std::string &devName, const std::string &ioName)
    {
        auto output_proxy = getOutputProxy(devName, ioName);

        for (auto &cname : output_proxy->getActiveConditions())
        {
            auto condition = saftlib::OutputCondition_Proxy::create(cname);
            if (condition->getDestructible() && (condition->getOwner() == ""))
            {
                condition->Destroy();
            }
        }
    }


void initializeSaftlib()
{
    // DBus initialization is performed in DU main before FESA switches to real-time threads
    // Gio::init();
    // Gio::DBus::Connection::get_sync(Gio::DBus::BUS_TYPE_SYSTEM);
    // This crashes inside glib when real-time threads are enabled

    try
    {
        auto timingLoop_ = Glib::MainLoop::create();

        // Get a list of devices from the saftlib directory
        std::map<std::string, std::string> timingDevices = saftlib::SAFTd_Proxy::create()->getDevices();
        // Create a proxy object in this process that has access to
        // the remote object stored inside the saftd daemon.

        // use the first receiver
        auto timingReceiver_ = saftlib::TimingReceiver_Proxy::create(timingDevices.begin()->second);

        // The timing sink name is "", so one is chosen automatically that does not conflict.
        auto timingSink_ = saftlib::SoftwareActionSink_Proxy::create(timingReceiver_->NewSoftwareActionSink(""));
        // SCU bus action sink to generate tags from events
        std::map<std::string, std::string> scus = timingReceiver_->getInterfaces()["SCUbusActionSink"];

        if (scus.size() == 1)
        {
                auto scubusTimingSink_ = saftlib::SCUbusActionSink_Proxy::create(scus.begin()->second);
        }
        else
        {
            // VME bus saftlib support to come
        }
    }
    catch (const Glib::Error& error)
    {
        std::ostringstream message;
        message << "Error initializing SAFTlib: " << error.what();
        throw RUNTIME_ERROR("Saft Error:" + message.str());
    }

}



void cb_copy_data(const float            *values,
                                        std::size_t             values_size,
                                        const float            *errors,
                                        std::size_t             errors_size,
                                        std::vector<gr::tag_t>& tags,
                                        void*                   userdata)
{
    //std::cout << "received some data ... too lazy to copy"  << std::endl;
}

// Returns the path of the folder where the executable is located
std::string executable_path()
{
    char result[PATH_MAX];
    ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
    std::string full_path(result, (count > 0) ? count : 0);
    std::size_t found = full_path.find_last_of("/\\");
    return full_path.substr(0,found);
}

std::string readFlowgraphFromFile (std::string path_in)
{
    std::string path = path_in;
    std::ifstream file1(path);
    if (!file1) // Does not exists ? --> Possibly a relative path --> Try to prefix with binary path
        path = executable_path() + "/" + path;
    file1.close();

    // LOG_ERROR_IF(logger, "grc_path: " + path);
    std::ifstream file2(path);
    if (!file2)
    {
        std::cout << "Can't open GRC file:'" + path + "'" << std::endl;
        exit(1);
    }

    std::stringstream buffer;
    buffer << file2.rdbuf();
    file2.close();

    std::cout << "Loading flowgraph from GRC file: " + path << std::endl;

    if (buffer.str().size() > MAX_FLOWGRAPH_XML_LENGTH)
    {
        std::cout << "Cant save xml file '" + path + "' in FESA. File exceeds MAX_FLOWGRAPH_XML_LENGTH." << std::endl;
        exit(1);
    }
    return buffer.str();
}

void throwDigitizerErrors(std::shared_ptr < flowgraph::FlowGraph > flowgraph)
{
    flowgraph->digitizers_apply(
        [](const std::string &id, gr::digitizers::digitizer_block *digitizer)
    {
        if(!digitizer->getConfigureExceptionMessage().empty())
        {
            std::cout << " Error in Difitizer Block: " << digitizer->getConfigureExceptionMessage() << std::endl;
            exit(1);
        }
    });
}

int main()
{
    //initializeSaftlib(); FIXME: Init Timing Does not work yet
    std::shared_ptr < flowgraph::FlowGraph > flowgraph;
    try
    {
        // Load flowgraph
        //std::string path = "dal007_V0.1.3.grc";
        std::string path = "minimum.grc";
        std::string flowGraph_string = readFlowgraphFromFile(path);
        //std::cout << "flowGraph_string_ " << flowGraph_string << std::endl;
        std::istringstream flowGraph_stream(flowGraph_string);
        flowgraph = flowgraph::make_flowgraph(flowGraph_stream);

        flowgraph->digitizers_apply(
            [&](const std::string &name, gr::digitizers::digitizer_block *digitizer)
        {
            std::cout << "Digitizer found" << std::endl;
            // 2MS for 20ms matching tolerance ... probably can be further pushed, e.g. when downsampling is used
            if( digitizer->get_acquisition_mode() == gr::digitizers::STREAMING&&  digitizer->get_samp_rate() < 2000000 )
            {
                std::cout << ": Minimum Digitizer samp_rate in streaming mode is 2MS. Otherwise event-matching might fail" << std::endl;
                exit(1);
            }
        });
    }
    catch(const std::exception& ex)
    {
        std::cout << ex.what() << std::endl;
        return 1;
    }
    catch(std::string err)
    {
        std::cout << " Error while making flowgraph: " << err << std::endl;
        return 1;
    }
    catch(...)
    {
        std::cout << "Unknown Error while making flowgraph." << std::endl;
        return 1;
    }
    /*  FIXME: Init Timing Does not work yet
    try
    {
        std::vector < std::string > events = {"CMD_SEQ_START#257", "CMD_BEAM_INJECTION#283", "CMD_BEAM_EXTRACTION#284", "CMD_START_ENERGY_RAMP#285", "CMD_CUSTOM_DIAG_1#286", "CMD_CUSTOM_DIAG_2#287", "CMD_FG_START#513"};
        auto ftrnName = "tr0";
        auto ioName = "IO3";
        double pulsewidth = 0.005;
        auto pulseWidthNanoseconds = static_cast<int64_t>(1000000000.0 * pulsewidth);

        int32_t groupId = 300; // SIS18

        destroyOutputConditions(ftrnName, ioName);
        configureOutput(ftrnName, ioName);

        for (auto t : events)
        {
            auto eventNumber = extractEventID(t);
            auto mask = generate_event_id_mask(groupId, eventNumber);
            auto event = generate_event_id(groupId, eventNumber);

            // Rising edge
            createOutputCondition(ftrnName, ioName, event, mask, 0, true);
            // Falling edge
            createOutputCondition(ftrnName, ioName, event, mask, pulseWidthNanoseconds, false);
        }
    }
    catch(const std::exception& ex)
    {
        std::cout << ex.what() << std::endl;
        return 1;
    }*/

    try
    {
        flowgraph->time_domain_sinks_apply(
            [&](const std::string &sink_name, gr::digitizers::time_domain_sink *sink)
        {
            // set callback for each sink
            sink->set_callback(cb_copy_data, NULL );
        });
        printf("main 1\n");
        flowgraph->start();
        throwDigitizerErrors (flowgraph);
        printf("main 2\n");
        flowgraph->stop();
        printf("main 3\n");
        flowgraph->wait();
        printf("main 4\n");
        flowgraph->start();
        printf("main 5\n");
    }
    catch(const std::exception& ex)
    {
        std::cout << ex.what() << std::endl;
        return 1;
    }
    catch(std::string err)
    {
        std::cout << " Error in grc file: " << err << std::endl;
        return 1;
    }
    catch(...)
    {
        std::cout << "Unknown Error in grc File." << std::endl;
        return 1;
    }

    std::cout << "Finished! - going to sleep now" << std::endl;
    while(true)
    {
        sleep(1);
    }
    return 0;
}

