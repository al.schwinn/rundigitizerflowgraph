## Help on how to modify Makefiles you can get e.g. here: http://www.ijon.de/comp/tutorials/makefile.html

#CPU ?= i686
CPU=x86_64

# No debug symbols, Prod version:
COMPILER_FLAGS += -I/opt/gnuradio/include

SAFT_HOME ?= /common/export/timing-rte/tg-enigma-v5.0.1-alpha/x86_64

DIGITIZER_VERSION = master
FLOWGRAPH_VERSION = master
DIGITIZER_HOME = /common/usr/cscofe/opt/gr-digitizer/${DIGITIZER_VERSION}
FLOWGRAPH_HOME = /common/usr/cscofe/opt/gr-flowgraph/${FLOWGRAPH_VERSION}
#For local development
#DIGITIZER_HOME = $(HOME)/git/gr-digitizers/build
#FLOWGRAPH_HOME = $(HOME)/git/gr-flowgraph/build


COMPILER_FLAGS += -I${DIGITIZER_HOME}/include
COMPILER_FLAGS += -I${FLOWGRAPH_HOME}/include
COMPILER_FLAGS += -isystem/usr/include/libxml2
COMPILER_FLAGS += -I$(SAFT_HOME)/include/saftlib

COMPILER_FLAGS += -I/usr/include/giomm-2.4 -I/usr/lib64/giomm-2.4/include
COMPILER_FLAGS += -I/usr/include/glibmm-2.4 -I/usr/lib64/glibmm-2.4/include
COMPILER_FLAGS += -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include -I/usr/include/sigc++-2.0
COMPILER_FLAGS += -I/usr/lib64/sigc++-2.0/include

LINKER_FLAGS += -L$(BOOST_HOME)/lib/$(CPU)
LINKER_FLAGS += -Wl,-Bstatic -lboost_thread -lboost_system -lboost_atomic -lboost_chrono -lboost_filesystem -lboost_program_options \
-Wl,-Bdynamic -lm -lrt -ldl -lxml2
LINKER_FLAGS += -Wl,-Bstatic -L$(SAFT_HOME)/lib -lsaftlib -lsaftbus -lsaftcommon
LINKER_FLAGS += -Wl,-Bdynamic -L/usr/lib64 -lgiomm-2.4 -lgio-2.0 -lglibmm-2.4 -lgmodule-2.0 -lgobject-2.0 -lsigc-2.0 -lgthread-2.0 -lrt -lglib-2.0

LINKER_FLAGS += -L/usr/lib64 -lboost_thread
LINKER_FLAGS += -L/opt/gsi/3rdparty/boost/1.54.0/lib/x86_64 -lboost_thread


LINKER_FLAGS += -Wl,-Bdynamic -L/opt/picoscope/lib
LINKER_FLAGS += -Wl,-Bdynamic -L/usr/lib64
LINKER_FLAGS += -Wl,-rpath='${DIGITIZER_HOME}/lib64',-L${DIGITIZER_HOME}/lib64
LINKER_FLAGS += -Wl,-rpath='${FLOWGRAPH_HOME}/lib64',-L${FLOWGRAPH_HOME}/lib64


LINKER_FLAGS += -lgnuradio-flowgraph
LINKER_FLAGS += -lgnuradio-blocks
LINKER_FLAGS += -lgnuradio-analog
LINKER_FLAGS += -lgnuradio-digitizers
LINKER_FLAGS += -lgnuradio-runtime
LINKER_FLAGS += -lgnuradio-pmt
LINKER_FLAGS += -lvolk

# ROOT Framework
LINKER_FLAGS += -Wl,-rpath='/opt/cern/root/lib',-L/opt/cern/root/lib
LINKER_FLAGS += -lTreePlayer -lCore -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lTree -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -lMultiProc

LIBS += -pthread
#allows to compile 32Bit binary on 64Bit processor
#COMPILER_FLAGS += -m32
#LINKER_FLAGS += -m32

#Optimisation flag ( 00 till 03 )
COMPILER_FLAGS += -O3 -fno-omit-frame-pointer

# Warn all - always use this to make the compiler really picky (and thus more helpful) 
COMPILER_FLAGS += -Wall -std=c++11

#include debugging symbols - always use this or your debugger doesn't work! 
#COMPILER_FLAGS += -g

# files to work with:
SOURCES = 	RunFlowgraph.cpp
TARGET =	RunFlowgraph

# .o files have same name as .cpp files
OBJS = $(SOURCES:%.cpp=%.o)

# if you want to build a library, this one is needed
LIBRARY =	lib$(TARGET).a

# Link command to compile + build binary:
link:		$(OBJS)
	g++ $(OBJS) $(LINKER_FLAGS) -o $(TARGET) $(LIBS)

# Command to build library instead of binary
lib:	$(OBJS)
	ar rcs $(LIBRARY) $(OBJS)

# Compilation command:
# Generic Way to create a .o file for each .cpp file (for many cpp files) $< is the "first dependency"
%.o: %.cpp 
	g++ $(INCLUDES) $(COMPILER_FLAGS) -c $<

#support for "make all"
all: link
	@echo "-- build of target: '$(TARGET)' finished --"

#support for "make clean"
clean:
	rm -f $(OBJS) $(TARGET) $(LIBRARY)

#if you just type "make", the first command in the list of commands is executed

# Simple Compilation command:
#compile:	$(CPP)
#	g++ $(INCLUDES) $(COMPILER_FLAGS) -c $(CPP) -o $(OBJS)
